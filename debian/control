Source: ruby-cancancan
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Utkarsh Gupta <guptautkarsh2102@gmail.com>
Build-Depends: debhelper-compat (= 13),
               gem2deb
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-cancancan.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-cancancan
Homepage: https://github.com/CanCanCommunity/cancancan
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-cancancan
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby:any | ruby-interpreter,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Authorization Gem for Ruby on Rails
 CanCanCan is an authorization library for Ruby >= 2.2.0 and Ruby on Rails
 >= 4.2 which restricts what resources a given user is allowed to access.
 .
 All permissions can be defined in one or multiple ability files and not
 duplicated across controllers, views, and database queries, keeping your
 permissions logic in one place for easy maintenance and testing.
 .
 It consists of two main parts:
  1. Authorizations library that allows you to define the rules to access
 different objects, and provides helpers to check for those permissions.
  2. Rails helpers to simplify the code in Rails Controllers by performing the
 loading and checking of permissions of models automatically and reduce
 duplicated code.
